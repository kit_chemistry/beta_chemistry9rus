//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [];

//AWARDs NUMBER
var awardNum = 0;

var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'g')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b class='lightgreen'>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b class='lightblue'>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var launch000 = function()
{
	
}

var launch101 = function()
{
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth");
	
	guideType = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	guideCloud.fadeOut(0);
	fadeNavsIn();
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideCloud.fadeIn(0);
			guideType.write();
		}, 2000);
	};
}

var launch102 = function()
{
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guide = $(prefix + ".s3-guide"),
		boyCloud = $(prefix + ".s3-boy-cloud"),
		guideCloud = $(prefix + ".s3-guide-cloud"),
		boyMouth = $(prefix + ".s3-boy-mouth"),
		guideMouth = $(prefix + ".s3-guide-mouth");
		capricorn = $(prefix + ".s2-golden-capricorn");
	
	typingBoy = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		guide.fadeIn(0);
		timeout[0] = setTimeout(function(){
			capricorn.fadeIn(0);
			guideCloud.fadeIn(0);
			guideMouth.fadeIn(0);
			typingGuide.write();
		},1000);
	});
		
	typingGuide = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	guideCloud.fadeOut(0);
	boyCloud.fadeOut(0);
	guide.fadeOut(0);
	boyMouth.fadeOut(0);
	guideMouth.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyCloud.fadeIn(0);
			boyMouth.fadeIn(0);
			typingBoy.write();
		}, 2000);
	};
	
}

var launch103 = function()
{
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud = $(prefix + ".boy-cloud"),
		guide = $(prefix + ".guide"),
		guideCloud = $(prefix + ".guide-cloud"),
		boyMouth = $(prefix + ".boy-mouth"),
		guideMouth = $(prefix + ".guide-mouth"),
		guideEyes = $(prefix + ".guide-eyes");
	
	boyType = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		guide.fadeIn(0);
		timeout[0] = setTimeout(function(){
			guideCloud.fadeIn(0);
			guideMouth.fadeIn(0);
			guideEyes.fadeIn(0);
			guideType.write();
		}, 500);
	});
	
	guideType = new TypingText(guideCloud, 100, false, function(){
			guideMouth.fadeOut(0);

		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	boyCloud.fadeOut(0);
	guide.fadeOut(0);
	guideCloud.fadeOut(0);
	boyMouth.fadeOut(0);
	guideMouth.fadeOut(0);
	guideEyes.fadeOut(0);
	fadeNavsIn();
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyCloud.fadeIn(0);
			boyType.write();
			boyMouth.fadeIn(0);
		}, 2000);
	};
}

var launch104 = function()
{
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		guideEyes = $(prefix + ".guide-eyes"), 
		corrosionLabel = $(prefix + ".corrosion-label");
	
	guideType = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	fadeNavsIn();
	guideEyes.fadeOut(0);
	guideMouth.fadeOut(0);
	guideCloud.fadeOut(0);
	corrosionLabel.fadeOut(0);
		
	startButtonListener = function(){
		guideType.write();
		guideMouth.fadeIn(0);
		corrosionLabel.fadeIn(1000);
		guideCloud.fadeIn(0);

	};
}

var launch105 = function()
{
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		taskCloud = $(prefix + "#s6-task-cloud"),
		items = $(prefix + ".item"),
		award = $(prefix + ".award"),
		mistakeNum = 0;
	
	fadeNavsIn();
	award.fadeOut(0);
	
	var taskType = new TypingText(taskCloud, 100, false, function(){
	});
	
	var itemListener = function(){
		currItem = $(this)
		if (currItem.attr("data-correct")) {
			currItem.css("border", "5px solid blue");
			timeout[0] = setTimeout(function(){
				currItem.fadeOut(1000);
				currItem.remove();
				var leftItems = $(prefix + ".metal");
				if(leftItems.length <= 0)
				{
					if (mistakeNum <= 1) {
						awardNum++;
						award.fadeIn(0);
						timeout[1] = setTimeout(function(){
							award.fadeOut(0);
							fadeNavsIn();
						}, 3000);
					}
					else
					{
						fadeNavsIn();
					}
				}
			}, 1000)
		}
		else
		{
			currItem.css("border", "5px solid red");
			timeout[0] = setTimeout(function(){
				currItem.css("border", "");
			}, 1000)
			mistakeNum++;
		}
	};
	
	items.off("click", itemListener);
	items.on("click", itemListener);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			taskType.write();
		}, 1000);
	};
}

var launch106 = function()
{
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlCloud = $(prefix + ".girl-cloud"),
		girlMouth = $(prefix + ".girl-mouth"),
		items = $(prefix + ".item"),
		question = $(prefix + ".question");
	
	guideType = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		question.fadeIn(500);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	fadeNavsIn();
	girlMouth.fadeOut(0);
	items.fadeOut(0);
	question.fadeOut(0);
	girlCloud.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlCloud.fadeIn(0);
			guideType.write();
			girlMouth.fadeIn(0);
			timeout[1] = setTimeout(function(){
				items.fadeIn(500);
			}, 3000);
		}, 1000);
	};
}

var launch107 = function()
{
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		teacherEyes = $(prefix + ".teacher-eyes"),
		series = $(prefix + ".series");
	
	teacherSprite = new Motio(teacherMouth[0], {
		"fps": "5",
		"frames": "2"
	});
	
	teacherEyesSprite = new Motio(teacherEyes[0], {
		"fps": "5",
		"frames": "2"
	});
	teacherEyesSprite.on("frame", function(){
		var currFrame = this;
		if (currFrame.frame === 0)
		{
			currFrame.pause();
			timeout[1] = setTimeout(function(){
				currFrame.play();
			}, 500);
		}
	});
	
	teacherType = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		teacherSprite.pause();
		timeout[2] = setTimeout(function(){
			fadeNavsIn();
		}, 6000);
	});
	
	teacherMouth.fadeOut(0);
	teacherCloud.fadeOut(0);
	series.fadeOut(0);
	fadeNavsIn();
	
	teacherEyesSprite.play();
		
	startButtonListener = function(){
		teacherType.write();
		teacherCloud.fadeIn(0);
		teacherMouth.fadeIn(0);
		teacherSprite.play();
		timeout[0] = setTimeout(function(){
			series.fadeIn(500);
		}, 5000);
	};
}

var launch108 = function()
{
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud1 = $(prefix + ".guide-cloud-1"),
		guideCloud2 = $(prefix + ".guide-cloud-2"),
		guideMouth = $(prefix + ".guide-mouth"),
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket");
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.css("background-image", vegetable.css("background-image"));
		basket.css("background-size", "100% 100%");
		vegetable.remove();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		alert("Ошибочка!");

	}
	
	var finishCondition = function()
	{
		return !$(prefix + ".ball").length;
	}
	
	var finishFunction = function(vegetable, basket)
	{
		fadeNavsIn();
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	guideType1 = new TypingText(guideCloud1, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			guideCloud1.fadeOut(0);
			guideCloud2.fadeIn(0);
			guideType2.write();
			guideMouth.fadeIn(0);
			blink(balls, 500, 4);
		}, 2000);
		timeout[1] = setTimeout(function(){
			blink(baskets, 500, 4);
		}, 5000);
	});
	
	guideType2 = new TypingText(guideCloud2, 100, false, function(){
		guideMouth.fadeOut(0);
	});
	
	fadeNavsIn();
	guideMouth.fadeOut(0);
	guideCloud1.fadeOut(0);
	guideCloud2.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideType1.write();
			guideMouth.fadeIn(0);
			guideCloud1.fadeIn(0);
		}, 1000);
	};
}

var launch109 = function()
{
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherCloud2 = $(prefix + ".teacher-cloud-2"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		beketovPortrait = $(prefix + ".beketov-portait"),
		beketovNameYears = $(prefix + ".beketov-name-years"),
		blueHighlight = $(prefix + ".series-blue-highlight"),
		greenHighlight = $(prefix + ".series-green-highlight"),
		push = $(prefix + ".push"),
		nopush = $(prefix + ".no-push");
	
	var teacherType = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			teacherCloud.fadeOut(0);
			teacherCloud2.fadeIn(0);
			teacherMouth.fadeIn(0);
			teacher2Type.write();
			timeout[1] = setTimeout(function(){
				push.fadeIn(0);
				greenHighlight.fadeIn(0);
			},11000);
			timeout[2] = setTimeout(function(){
				greenHighlight.fadeOut(0);
				nopush.fadeIn(0);
				blueHighlight.fadeIn(0);
			},18000);
		}, 5000);
	});
	
	var teacher2Type = new TypingText(teacherCloud2, 100, false, function(){
		teacherMouth.fadeOut(0);
		greenHighlight.fadeIn(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 7000);
	});
	
	fadeNavsIn();
	teacherCloud.fadeOut(0);
	teacherCloud2.fadeOut(0);
	blueHighlight.fadeOut(0);
	greenHighlight.fadeOut(0);
	push.fadeOut(0);
	nopush.fadeOut(0);
	teacherMouth.fadeOut(0);
	beketovPortrait.fadeOut(0);
	beketovNameYears.fadeOut(0);
		
	startButtonListener = function(){
		teacherCloud.fadeIn(0);
		teacherType.write();
		teacherMouth.fadeIn(0);
		timeout[4] = setTimeout(function(){
			beketovPortrait.fadeIn(500);	
		}, 3000);
		timeout[5] = setTimeout(function(){
			beketovNameYears.fadeIn(500);	
		}, 4000);
	};
}

var launch110 = function()
{
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud = $(prefix + ".boy-cloud"),
		boyMouth = $(prefix + ".boy-mouth"),
		ring = $(prefix + ".ring"),
		spoon = $(prefix + ".spoon"), 
		door = $(prefix + ".door"),
		caserole = $(prefix + ".caserole"),
		cable = $(prefix + ".cable"),
		natrium = $(prefix + ".natrium"),
		kalium = $(prefix + ".kalium"),
		calcium = $(prefix + ".calcium"),
		items = $(prefix + ".item");
	
	var boyType = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[8] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	boyCloud.fadeOut(0);
	boyMouth.fadeOut(0);
	items.fadeOut(0);
	fadeNavsIn();
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyCloud.fadeIn(0);
			boyType.write();
			boyMouth.fadeIn(0);
		}, 1000);
		timeout[0] = setTimeout(function(){
			ring.fadeIn(500);
		}, 6000);
		timeout[1] = setTimeout(function(){
			spoon.fadeIn(500);
		}, 7000);
		timeout[2] = setTimeout(function(){
			door.fadeIn(500);
		}, 8000);
		timeout[3] = setTimeout(function(){
			caserole.fadeIn(500);
		}, 9000);
		timeout[4] = setTimeout(function(){
			cable.fadeIn(500);
		}, 10000);
		timeout[5] = setTimeout(function(){
			natrium.fadeIn(500);
		}, 14000);
		timeout[6] = setTimeout(function(){
			kalium.fadeIn(500);
		}, 15000);
		timeout[7] = setTimeout(function(){
			calcium.fadeIn(500);
		}, 16000);
	};
}

var launch111 = function()
{
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideMouth = $(prefix + ".guide-mouth"),
		activeHighlight = $(prefix + ".active-metals-highlight"),
		midActiveHighlight = $(prefix + ".middle-metals-highlight"),
		inactiveHighlight = $(prefix + ".inactive-metals-highlight"),
		guideCloud1 = $(prefix + ".guide-cloud-1"),
		guideCloud2 = $(prefix + ".guide-cloud-2"),
		guideCloud3 = $(prefix + ".guide-cloud-3"),
		active = $(prefix + ".active"),
		middleActive = $(prefix + ".middle-active"),
		inactive = $(prefix + ".inactive");
	
	guideType1 = new TypingText(guideCloud1, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[1] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud2.fadeIn(0);
			guideType2.write();
			timeout[2] = setTimeout(function(){
				midActiveHighlight.fadeIn(500);
				middleActive.fadeIn(500);
			}, 2000);
		}, 1000);
	});
	guideType2 = new TypingText(guideCloud2, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[3] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud3.fadeIn(0);
			guideType3.write();
			timeout[4] = setTimeout(function(){
				inactiveHighlight.fadeIn(500);
				inactive.fadeIn(500);
			}, 2000);
		}, 1000);
	});
	guideType3 = new TypingText(guideCloud3, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[5] = setTimeout(function(){
			fadeNavsIn();
		}, 6000);
	});
	
	fadeNavsIn();
	guideMouth.fadeOut(0);
	activeHighlight.fadeOut(0);
	midActiveHighlight.fadeOut(0);
	inactiveHighlight.fadeOut(0);
	guideCloud1.fadeOut(0);
	guideCloud2.fadeOut(0);
	guideCloud3.fadeOut(0);
	active.fadeOut(0);
	middleActive.fadeOut(0);
	inactive.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideCloud1.fadeIn(0);
			guideType1.write();
			guideMouth.fadeIn(0);
		}, 1000);
				
		timeout[0] = setTimeout(function(){
			active.fadeIn(500);
			activeHighlight.fadeIn(500);
		}, 5000);
	};
}

var launch112 = function()
{
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		videoContainer = $(prefix + ".video-container"),
		experiment = $(prefix + ".experiment");
	
	experiment.attr("width", videoContainer.css("width"));
	experiment.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	});
	
	teacherType = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			videoContainer.css("width", "100%");
			videoContainer.css("height", "100%");
			videoContainer.css("left", "0%");
			videoContainer.css("top", "0%");
			experiment.attr("width", videoContainer.css("width"));
			experiment.attr("height", videoContainer.css("height"));
			experiment[0].play();
		}, 3000);
	});
	
	fadeNavsIn();
	teacherMouth.fadeOut(0);
	
	experiment[0].addEventListener("ended", function(){
		fadeNavsIn();
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			teacherType.write();
			teacherMouth.fadeIn(0);
		}, 1000);
	};
}

var launch113 = function()
{
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud1 = $(prefix + ".boy-cloud-1"),
		boyCloud2 = $(prefix + ".boy-cloud-2"),
		boyMouth = $(prefix + ".boy-mouth"),
		boxes = $(prefix + ".box"),
		award = $(prefix + ".award"),
		mistakesNum = 0,
		correctNum = 0;
	
	boyType1 = new TypingText(boyCloud1, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boyCloud2.fadeIn(0);
			boyType2.write();
			boyMouth.fadeIn(0);
		}, 1000);
	});
	boyType2 = new TypingText(boyCloud2, 100, false, function(){
		boyMouth.fadeOut(0);
		boxes.fadeIn(1000);
	});
	
	var boxListener = function(){
		currBox = $(this);
		if (currBox.attr("data-correct"))
		{
			currBox.css("background-color", "blue");
			currBox.css("color", "white");
			currBox.off("click", boxListener);
			correctNum ++;
		}
		else
		{
			currBox.css("background-color", "red");
			currBox.css("color", "white");
			mistakesNum ++;
			timeout[1] = setTimeout(function(){
				currBox.css("background-color", "");
				currBox.css("color", "");
			}, 1000);
		}
		if (correctNum >= 5)
		{
			if (mistakesNum <= 1) {
				awardNum++;
				award.fadeIn(500);
				timeout[2] = setTimeout(function(){
					award.fadeOut(0);
					fadeNavsIn();
				}, 4000);
			}
			else
			{
				fadeNavsIn();
			}
		}
	}
	
	boxes.off("click", boxListener);
	boxes.on("click", boxListener);
	
	fadeNavsIn();
	boyCloud1.fadeOut(0);
	boyCloud2.fadeOut(0);
	boyMouth.fadeOut(0);
	boxes.fadeOut(0);
	award.fadeOut(0);
		
	startButtonListener = function(){
		boyCloud1.fadeIn(0);
		boyType1.write();
		boyMouth.fadeIn(0);
	};
}

var launch114 = function()
{
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		girlCloud = $(prefix + ".girl-cloud"),
		boyCloud = $(prefix + ".boy-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		girlMouth = $(prefix + ".girl-mouth"),
		boyMouth = $(prefix + ".boy-mouth");
	
	guideType = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			girlMouth.fadeIn(0);
			girlCloud.fadeIn(0);
			girlType.write();
		}, 1000);
	});
	boyType = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud.fadeIn(0);
			guideType.write();
		}, 1000);
	});
	girlType = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	fadeNavsIn();
	boyCloud.fadeOut(0);
	girlCloud.fadeOut(0);
	guideCloud.fadeOut(0);
	girlMouth.fadeOut(0);
	guideMouth.fadeOut(0);
	boyMouth.fadeOut(0);
		
	startButtonListener = function(){
		boyType.write();
		boyCloud.fadeIn(0);
		boyMouth.fadeIn(0);
	};
}

var launch115 = function()
{
		theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		boxes = $(prefix + ".box"),
		part1 = $(prefix + ".part-1"),
		part2 = $(prefix + ".part-2"),
		example = $(prefix + ".s15-for-example");
		
	guideType = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		fadeOneByOne(part1, 0, 500, function(){});

		timeout[0] = setTimeout(function () {
			example.fadeIn(0);
		}, 6000);

		timeout[0] = setTimeout(function(){
			fadeOneByOne(part2, 0, 500, function(){});
		}, 6000);

		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 15000);
	});
	
	fadeNavsIn();
	guideCloud.fadeOut(0);
	guideMouth.fadeOut(0);
	part1.fadeOut(0);
	part2.fadeOut(0);
	example.fadeOut(0);
		
	startButtonListener = function(){
		guideCloud.fadeIn(0);
		guideType.write();
		guideMouth.fadeIn(0);
	};
}

var launch116 = function()
{
		theFrame = $("#frame-116"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud1 = $(prefix + ".girl-cloud-1"),
		guideCloud2 = $(prefix + ".girl-cloud-2"),
		guideMouth = $(prefix + ".guide-mouth"),
		girlMouth = $(prefix + ".girl-mouth"),
		questions = $(prefix + ".questions"),
		boxes = $(prefix + ".box"),
		balls = $(prefix + ".ball"),
		mistakesNum = 0,
		award = $(prefix + ".award");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var failFunction = function(vegetable, basket){
		mistakesNum ++;
		alert("Ошибок: "+mistakesNum);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var finishCondition = function(){
		return allHaveHtml($(prefix + ".basket")); 
	};
	
	var finishFunction = function(){
		if (mistakesNum <= 2)
		{
			awardNum ++;
			award.fadeIn(500);
			timeout[0] = setTimeout(function(){
				award.fadeOut(0);
				fadeNavsIn();
			}, 3000);
		}
		else
			fadeNavsIn();
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition,finishFunction);
	
	girlType1 = new TypingText(guideCloud1, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			girlType2.write();
			girlMouth.fadeIn(0);
			guideCloud2.fadeIn(0);
		}, 2000);
	});
	
	girlType2 = new TypingText(guideCloud2, 100, false, function(){
		girlMouth.fadeOut(0);
		questions.fadeIn(1000);
		boxes.fadeIn(1000);
	});
	
	fadeNavsIn();
	guideCloud1.fadeOut(0);
	guideCloud2.fadeOut(0);
	girlMouth.fadeOut(0);
	award.fadeOut(0);
	questions.fadeOut(0);
	boxes.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlType1.write();
			guideCloud1.fadeIn(0);
			girlMouth.fadeIn(0);
		}, 1000);
	};
}

var launch117 = function()
{
		theFrame = $("#frame-117"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		boyCloud = $(prefix + ".boy-cloud"),
		boyMouth = $(prefix + ".boy-mouth"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		
		boy = $(prefix + ".boy"),
		boyEyes = $(prefix + ".boy-eyes"),

		girl = $(prefix + ".s17-main-girl"),
		girlCloud = $(prefix + ".s17-girl-cloud"),
		girlMouth = $(prefix + ".s17-girl-mouth"),
		girlEyes = $(prefix + ".s17-girl-eyes");
		
	teacherType = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boy.fadeIn(0);
			boyCloud.fadeIn(0);
			boyType.write();
			boyMouth.fadeIn(0);
			boyEyes.fadeIn(0);
		}, 1000);
	});
	
	boyType = new TypingText(boyCloud, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			girl.fadeIn(0);
			girlCloud.fadeIn(0);
			girlMouth.fadeIn(0);
			girlEyes.fadeIn(0);
			girlType.write();
		}, 1000);
	});
	
	var girlType = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});

	fadeNavsIn();

	teacherCloud.fadeOut(0);
	teacherMouth.fadeOut(0);

	boy.fadeOut(0);
	boyEyes.fadeOut(0);
	boyMouth.fadeOut(0);
	boyCloud.fadeOut(0);
	
	girl.fadeOut(0);
	girlMouth.fadeOut(0);
	girlEyes.fadeOut(0);
	girlCloud.fadeOut(0);

		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			teacherMouth.fadeIn(0);
			teacherCloud.fadeIn(0);
			teacherType.write();
		}, 1000);
	};
}

var launch118 = function()
{
		theFrame = $("#frame-118"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		guideEyes = $(prefix + ".guide-eyes"),
		guide = $(prefix + ".guide"),
		videoContainer = $(prefix + ".video-container"),
		experiment = $(prefix + ".video-container .video");

		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	});

	experiment[0].addEventListener("ended",function(){
		timeout[1] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);	
	});

	var guideType = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			videoContainer.fadeIn(0);
			experiment[0].play();
		}, 1500);
	});
	
	
	guideCloud.fadeOut(0);
	guideMouth.fadeOut(0);
	guide.fadeOut(0);
	guideEyes.fadeOut(0);
	videoContainer.fadeOut(0);
	fadeNavsIn();
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideMouth.fadeIn(0);
			guideCloud.fadeIn(0);
			guideType.write();	
		}, 1000);	
	};
}

var launch119 = function()
{
		theFrame = $("#frame-119"),
		theClone = theFrame.clone;
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		teacherEyes = $(prefix + ".teacher-eyes"),
		part1 = $(prefix + ".part-1"),
		part2 = $(prefix + ".part-2");
	
	teacherType = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeOneByOne(part1, 0, 500, function(){
				timeout[1] = setTimeout(function(){
					fadeOneByOne(part2, 0, 500, function(){
						timeout[2] = setTimeout(function(){
							fadeNavsIn();
						}, 5000);
					})
				}, 2000);
			});
		}, 2000);
	});
	
	fadeNavsIn();
	teacherEyes.fadeOut(0);
	teacherMouth.fadeOut(0);
	teacherCloud.fadeOut(0);
	part1.fadeOut(0);
	part2.fadeOut(0);
		
	startButtonListener = function(){
		teacherCloud.fadeIn(0);
		teacherMouth.fadeIn(0);
		teacherMouth.fadeIn(0);

		teacherType.write();
	};
}

var launch120 = function()
{
		theFrame = $("#frame-120"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyCloud1 = $(prefix + ".boy-cloud-1"),
		boyCloud2 = $(prefix + ".boy-cloud-2"),
		boyMouth = $(prefix + ".boy-mouth"),
		balls = $(prefix + ".ball"),
		boxes = $(prefix + ".box"),
		question = $(prefix + ".question"),
		mistakesNum = 0,
		award = $(prefix + ".award");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var failFunction = function(vegetable, basket){
		mistakesNum ++;
		alert("Ошибок: "+ mistakesNum);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var finishCondition = function(){
		return allHaveHtml($(prefix + ".basket")); 
	};
	
	var finishFunction = function(){
		if (mistakesNum <= 2)
		{
			awardNum ++;
			award.fadeIn(500);
			timeout[0] = setTimeout(function(){
				award.fadeOut(0);
				fadeNavsIn();
			}, 3000);
		}
		else
			fadeNavsIn();
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition,finishFunction);
	
	var boyType1 = new TypingText(boyCloud1, 100, false, function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			boyCloud1.fadeOut(0);
			boyCloud2.fadeIn(0);
			boyType2.write();
		}, 2000);
	});
	
	var boyType2 = new TypingText(boyCloud2, 100, false, function(){
		boyMouth.fadeOut(0);
		boxes.fadeIn(1000);
		question.fadeIn(1000);
	});
	
	fadeNavsIn();
	boyMouth.fadeOut(0);
	boyCloud1.fadeOut(0);
	boyCloud2.fadeOut(0);
	award.fadeOut(0);
	boxes.fadeOut(0);
	question.fadeOut(0);
		
	startButtonListener = function(){
		boyCloud1.fadeIn(0);
		boyType1.write();
		boyMouth.fadeIn(0);
	};
}

var launch120b = function()
{
		theFrame = $("#frame-120-b"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		balls = $(prefix + ".ball"),
		boxes = $(prefix + ".box"),
		question = $(prefix + ".question"),
		mistakesNum = 0,
		award = $(prefix + ".award");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var failFunction = function(vegetable, basket){
		mistakesNum ++;
		alert("Ошибок: "+ mistakesNum);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	};
	
	var finishCondition = function(){
		return allHaveHtml($(prefix + ".basket")); 
	};
	
	var finishFunction = function(){
		if (mistakesNum <= 2)
		{
			blackSkin.fadeIn(0);
			awardNum++;
			award.fadeIn(500);
			timeout[0] = setTimeout(function(){
				award.fadeOut(0);
				fadeNavsIn();
			}, 3000);
		}
		fadeNavsIn();
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition,finishFunction);
	
	fadeNavsIn();
	award.fadeOut(0);
	boxes.fadeOut(0);
	question.fadeOut(0);
		
	startButtonListener = function(){
		boxes.fadeIn(1000);
		question.fadeIn(1000);
	};
}

var launch121 = function()
{
		theFrame = $("#frame-121"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlCloud = $(prefix + ".girl-cloud"),
		girlMouth = $(prefix + ".girl-mouth");
	
	girlType = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	fadeNavsIn();
	girlCloud.fadeOut(0);
	girlMouth.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlMouth.fadeIn(0);
			girlCloud.fadeIn(0);
			girlType.write();
		}, 1000);
	};
}

var launch122 = function()
{
		theFrame = $("#frame-122"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideMouth = $(prefix + ".guide-mouth"),
		videoContainer = $(prefix + ".video-container"),
		experiment = $(prefix + ".experiment");
	
	experiment.attr("width", videoContainer.css("width"));
	experiment.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		experiment.attr("width", videoContainer.css("width"));
		experiment.attr("height", videoContainer.css("height"));
	});
	
	guideType = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			videoContainer.css("width", "100%");
			videoContainer.css("height", "100%");
			videoContainer.css("left", "0%");
			videoContainer.css("top", "0%");
			experiment.attr("width", videoContainer.css("width"));
			experiment.attr("height", videoContainer.css("height"));
			experiment[0].play();
		}, 3000);
	});
	
	experiment[0].addEventListener("ended", function(){
		fadeNavsIn();
	});
	
	fadeNavsIn();
	guideCloud.fadeOut(0);
	guideMouth.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			guideType.write();
			guideCloud.fadeIn(0);
			guideMouth.fadeIn(0);
		}, 1000);
	};
}

var launch123 = function()
{
		theFrame = $("#frame-123"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		guideCloud = $(prefix + ".guide-cloud"),
		guideEyes = $(prefix + ".guide-eyes"),
		guide = $(prefix + ".guide"),
		guideMouth = $(prefix + ".guide-mouth"),
		girlCloud = $(prefix + ".girl-cloud"),
		girlMouth = $(prefix + ".girl-mouth");
	
	guideType = new TypingText(guideCloud, 100, false, function(){
		guideMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	girlType = new TypingText(girlCloud, 100, false, function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			guide.fadeIn(0);
			guideCloud.fadeIn(0);
			guideEyes.fadeIn(0);
			guideType.write();
			guideMouth.fadeIn(0);
		}, 2000);
	});
	
	fadeNavsIn();
	girlMouth.fadeOut(0);
	girlCloud.fadeOut(0);
	guideMouth.fadeOut(0);
	guideEyes.fadeOut(0);
	guide.fadeOut(0);
	guideCloud.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlCloud.fadeIn(0);
			girlMouth.fadeIn(0);
			girlType.write();
		}, 1000);
	};
}

var launch124 = function()
{
		theFrame = $("#frame-124"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		textfields = $(prefix + ".textfield"),
		nextStep = $(prefix + ".next-step-button"),
		prevStep = $(prefix + ".prev-step-button"),
		saveStep = $(prefix + ".save-step-button"),
		steps = $(prefix + ".step"),
		activeStepNum = 1,
		activeStep = $(prefix + ".step-" + activeStepNum);
	
	textfields.val("");
	
	var teacherType = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			activeStep.fadeIn(500);
		}, 1000);
	});
	
	var nextStepListener = function(){
		if (activeStepNum <= 3) 
			activeStepNum ++;
		
		activeStep = $(prefix + ".step-" + activeStepNum);
		steps.fadeOut(0);
		activeStep.fadeIn(500);
	}
	nextStep.off("click", nextStepListener);
	nextStep.on("click", nextStepListener);
	
	var prevStepListener = function(){
		if (activeStepNum >= 0) 
			activeStepNum --;
		activeStep = $(prefix + ".step-" + activeStepNum);
		steps.fadeOut(0);
		activeStep.fadeIn(500);
	}
	prevStep.off("click", prevStepListener);
	prevStep.on("click", prevStepListener);	
	
	var saveStepListener = function(){
		if (activeStepNum >= 0) 
			activeStepNum ++;
		activeStep = $(prefix + ".step-" + activeStepNum);
		steps.fadeOut(0);
		activeStep.fadeIn(500);
		
		var doc = new jsPDF();
		doc.text(20, 20, $(textfields[0]).val());
		doc.text(20, 30, $(textfields[1]).val());
		doc.text(20, 40, $(textfields[2]).val());
		
		doc.save('Test.pdf');
		fadeNavsIn();
	}
	saveStep.off("click", saveStepListener);
	saveStep.on("click", saveStepListener);	
	
	fadeNavsIn();
	teacherCloud.fadeOut(0);
	teacherMouth.fadeOut(0);
	steps.fadeOut(0);
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			teacherMouth.fadeIn(0);
			teacherCloud.fadeIn(0);
			teacherType.write();
		}, 1000);
	};
}

var launch125 = function()
{
		theFrame = $("#frame-125"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		teacherCloud = $(prefix + ".teacher-cloud"),
		teacherMouth = $(prefix + ".teacher-mouth"),
		boxes = $(prefix + ".box"),
		result = $(prefix + ".result");
	
	teacherType = new TypingText(teacherCloud, 100, false, function(){
		teacherMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boxes.fadeIn(1000);
		}, 1000);
	});
		
	var boxesListener = function()
	{
		if (awardNum > 0) {
			$(this).css("transform", "rotateY(0deg)");
			$(this).css("background", "white");
			$(this).css("color", "black");
			awardNum--;
		}
		else
		{
			result.fadeIn(0);
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				fadeNavsIn();
			}, 5000);
		}
	}
	boxes.off("click", boxesListener);
	boxes.on("click", boxesListener);
	
	fadeNavsIn();
	teacherCloud.fadeOut(0);
	result.fadeOut(0);
	teacherMouth.fadeOut(0);
	boxes.fadeOut(0);
	
	startButtonListener = function(){
		teacherMouth.fadeIn(0);
		teacherCloud.fadeIn(0);
		teacherType.write();
	};
}

var launch126 = function()
{
	var theFrame = $("#frame-115"),
		prefix = "#" + theFrame.attr("id") + " ";
		
	fadeNavsOut();
}

var launch127 = function()
{
	var theFrame = $("#frame-127"),
		prefix = "#" + theFrame.attr("id") + " ",
		answer = $(prefix + ".answer");
		
	var answerListener = function(){
		$(this).css("transform", "rotateY(0deg)");
		$(this).css("color", "black");
	}
	
	answer.off("click", answerListener);
	answer.on("click", answerListener);
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	$(".award-num .label").html(awardNum);
	$(".award-num").fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	switch(elem.attr("id"))
	{
		case "frame-000":
			launch000();
			fadeNavsOut();
			fadeLauncherOut();
			$(".award-num").fadeOut(0);
			break;
		case "frame-101":
			launch101();
			break;
		case "frame-102":
			launch102();
			break;
		case "frame-103":
			launch103();
			break;
		case "frame-104":
			launch104();
			break;
		case "frame-105":
			launch105();
			break;
		case "frame-106":
			launch106();
			break;
		case "frame-107":
			launch107();
			break;
		case "frame-108":
			launch108();
			$(".award-num").fadeOut(0);
			break;
		case "frame-109":
			launch109();
			$(".award-num").fadeOut(0);
			break;
		case "frame-110":
			launch110();
			break;
		case "frame-111":
			launch111();
			$(".award-num").fadeOut(0);
			break;
		case "frame-112":
			launch112();
			break;
		case "frame-113":
			launch113();
			$(".award-num").fadeOut(0);
			break;
		case "frame-114":
			launch114();
			break;
		case "frame-115":
			launch115();
			break;
		case "frame-116":
			launch116();
			break;
		case "frame-117":
			launch117();
			break;
		case "frame-118":
			launch118();
			break;
		case "frame-119":
			launch119();
			break;
		case "frame-120":
			launch120();
			break;
		case "frame-120-b":
			launch120b();
			break;
		case "frame-121":
			launch121();
			break;
		case "frame-122":
			launch122();
			break;
		case "frame-123":
			launch123();
			break;
		case "frame-124":
			launch124();
			break;
		case "frame-125":
			launch125();
			break;
		case "frame-126":
			launch126();
			break;
		case "frame-127":
			launch127();
			break;
	}
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
};

$(document).ready(main);